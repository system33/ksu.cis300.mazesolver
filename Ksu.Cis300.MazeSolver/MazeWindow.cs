﻿/* MazeWindow.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ksu.Cis300.MazeLibrary;

namespace Ksu.Cis300.MazeSolver
{
    public partial class MazeWindow : Form
    {
        public MazeWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Given a starting position, find a path to
        /// an exit. Then draws the path.
        /// </summary>
        /// <param name="location">Starting position</param>
        private void drawPathToExit(Cell location)
        {
            const bool VISITED = true; // just to make marking cells as visited clearer

            Stack<Direction> movementHistory = new Stack<Direction>(); // stack of movement history to make backtracking possible
            bool[,] cells = new bool[uxMaze.MazeHeight, uxMaze.MazeWidth]; // holds bools for whether each spot has been visited
            
            Direction facingDirection = Direction.North; // initial direction to try
            cells[location.Row, location.Column] = VISITED; // mark initial position as visited

            while (uxMaze.IsInMaze(location)) // loop while inside the maze
            {
                // 0     < 1    < 2     < 3
                // North < East < South < West

                if (facingDirection <= Direction.West) // still have an untried dir, don't have to backtrack
                {
                    Cell forwardCell = uxMaze.Step(location, facingDirection); // what cell is in front of us

                    if (uxMaze.IsClear(location, facingDirection) && // if the cell we are facing is clear
                        (!uxMaze.IsInMaze(forwardCell) || cells[forwardCell.Row, forwardCell.Column] != VISITED)) // and it is either outside the maze or unvisited
                    {
                        // move to the forward cell

                        uxMaze.DrawPath(location, facingDirection); // draw path
                        location = forwardCell; // move current location forward
                        movementHistory.Push(facingDirection); // record the movement
                        facingDirection = Direction.North; // reset dir we are facing
                        if (uxMaze.IsInMaze(location)) // if we are in the maze still
                            cells[location.Row, location.Column] = VISITED; // mark new location as visited
                    }
                    else facingDirection++; // can't move that way, get ready to try a different direction
                }
                else if (movementHistory.Count > 0) // tried all dirs, but can backtrack
                {
                    // see what step we just made and move backwards

                    facingDirection = movementHistory.Pop(); // get previous direction
                    location = uxMaze.ReverseStep(location, facingDirection); // move opposite dir as what we are facing
                    uxMaze.ErasePath(location, facingDirection); // get rid of the path
                    facingDirection++; // advance to next direction to try
                }
                else // tried everything and can't backtrack anymore. Give up
                {
                    MessageBox.Show("There is no path from this cell.");
                    break;
                }
            }
        }

        /// <summary>
        /// Handles when the button is pressed.
        /// Generates a new maze.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxNewMazeButton_Click(object sender, EventArgs e)
        {
            uxMaze.Generate();
        }

        /// <summary>
        /// Handles when the maze is clicked on.
        /// Cleans up any previous drawn paths,
        /// calculates new path (if possible), and
        /// marks maze for redrawing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxMaze_MouseClick(object sender, MouseEventArgs e)
        {
            Cell clickedCell = uxMaze.GetCellFromPixel(e.Location);
            if (uxMaze.IsInMaze(clickedCell))
            {
                uxMaze.EraseAllPaths();
                drawPathToExit(clickedCell);
                uxMaze.Invalidate();
            }
        }
    }
}

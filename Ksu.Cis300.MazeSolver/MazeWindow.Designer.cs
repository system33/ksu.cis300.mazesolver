﻿namespace Ksu.Cis300.MazeSolver
{
    partial class MazeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxMaze = new Ksu.Cis300.MazeLibrary.Maze();
            this.uxNewMazeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // uxMaze
            // 
            this.uxMaze.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxMaze.Location = new System.Drawing.Point(12, 12);
            this.uxMaze.Name = "uxMaze";
            this.uxMaze.PathColor = System.Drawing.SystemColors.Highlight;
            this.uxMaze.Size = new System.Drawing.Size(260, 209);
            this.uxMaze.TabIndex = 0;
            this.uxMaze.MouseClick += new System.Windows.Forms.MouseEventHandler(this.uxMaze_MouseClick);
            // 
            // uxNewMazeButton
            // 
            this.uxNewMazeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.uxNewMazeButton.Location = new System.Drawing.Point(100, 227);
            this.uxNewMazeButton.Name = "uxNewMazeButton";
            this.uxNewMazeButton.Size = new System.Drawing.Size(75, 23);
            this.uxNewMazeButton.TabIndex = 1;
            this.uxNewMazeButton.Text = "New Maze";
            this.uxNewMazeButton.UseVisualStyleBackColor = true;
            this.uxNewMazeButton.Click += new System.EventHandler(this.uxNewMazeButton_Click);
            // 
            // MazeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.uxNewMazeButton);
            this.Controls.Add(this.uxMaze);
            this.Name = "MazeWindow";
            this.Text = "Maze Solver";
            this.ResumeLayout(false);

        }

        #endregion

        private MazeLibrary.Maze uxMaze;
        private System.Windows.Forms.Button uxNewMazeButton;
    }
}

